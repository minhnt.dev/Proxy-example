package org.phil;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.phil.entity.Employee;

public class MethodReferentEx {

	public static String CHARACTORNAME = "P";

	public static void main(String[] args) {
		List<String> lst = Arrays.asList("Join", "Phil", "Strick");

		// Lambda
		System.out.println("------Lambda-----");
		lst.forEach(s -> System.out.println(s));
		// Stream
		System.out.println("------Stream-----");
		filterWithCharacter(lst, "P").forEach(System.out::println);

		// Method Referent
		System.out.println("------Method Referent -----");
		lst.forEach(System.out::println);

		System.out.println("------Method Referent Static Method-----");
		lst.forEach(MethodReferentEx::filterWithCharacter);
		
		System.out.println("------Method Referent Contructor-----");
		filterWithCharacter(Employee::new, lst).forEach(System.out::println);
		
		//Lazy call peek() like forEach() but it's return to Stream
		System.out.println("------ Lazy call method--------");
		lst.stream().peek(System.out::println);
		
	}

	public static List<String> filterWithCharacter(List<String> lst, String c) {
		// Use Stream to filter
		List<String> lstFilter = lst.stream().filter(l -> l.indexOf(c) != -1).collect(Collectors.toList());
		return lstFilter;
	}

	public static void filterWithCharacter(String c) {
		if (c.indexOf(CHARACTORNAME) != -1) {
			System.out.println(c);
		}
	}


	public static <R> List<R> filterWithCharacter(Function<String, R> function, List<String> empName) {
		List<String> names = empName.stream().filter(e -> e.indexOf(CHARACTORNAME) != -1).collect(Collectors.toList());
		List<R> emps = new ArrayList<>();
		names.forEach(name -> emps.add(function.apply(name)));
		return emps;
	}
}
