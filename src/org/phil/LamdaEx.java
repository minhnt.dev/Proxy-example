package org.phil;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class LamdaEx {
	public static void main(String[] args) {
		long startTime=System.currentTimeMillis();
		List<String> lst=new ArrayList<>();
		lst.add("one");
		lst.add("fly");
		lst.add("opghd");
		System.out.println(lst.stream().filter(s -> s.length()==3).collect(Collectors.toList()));
		lst.forEach(s-> System.out.println(s));
		
//		List<String> tmp=new ArrayList<>();
//		for(String s:lst){
//			if(s.length()==3){
//				tmp.add(s);
//			}
//		}
//		System.out.println(tmp);
		System.out.println(System.currentTimeMillis()-startTime);
	}
}
